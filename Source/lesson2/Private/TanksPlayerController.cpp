// Fill out your copyright notice in the Description page of Project Settings.


#include "TanksPlayerController.h"

#include "TankPawn.h"

void ATanksPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();
	InputComponent->BindAxis("Movement", this, &ATanksPlayerController::OnMovement);
	InputComponent->BindAxis("Strafe", this, &ATanksPlayerController::OnStraif);
}

void ATanksPlayerController::BeginPlay()
{
	Super::BeginPlay();
	PlayerTank = CastChecked<ATankPawn>(GetPawn());
}

void ATanksPlayerController::OnMovement(float Value)
{
	if(PlayerTank)
	{
		PlayerTank->Movement(Value);
	}
}

void ATanksPlayerController::OnStraif(float Value)
{
	if(PlayerTank)
	{
		PlayerTank->Straif(Value);
	}
}
